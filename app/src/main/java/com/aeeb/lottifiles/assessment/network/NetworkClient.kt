package com.aeeb.lottifiles.assessment.network

import android.content.Context
import com.aeeb.lottifiles.assessment.network.interceptor.CacheInterceptor
import com.aeeb.lottifiles.assessment.network.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

class NetworkClient(context: Context, private val sessionAuthManager: SessionAuthManager) {
    private val gson = Gson()
    private val apiService: ApiService = Retrofit.Builder()
        .client(
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
                .addNetworkInterceptor(CacheInterceptor())
                .cache(Cache(File(context.cacheDir, "http-cache"), 20L * 1024 * 1024))
                .build()
        )
        .baseUrl("https://firebasestorage.googleapis.com/v0/b/lottiefiles-test.appspot.com/o/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(ApiService::class.java)

    fun login() {
        sessionAuthManager.featuredAuthToken = "f6e406f5-befb-40ab-a9b0-bb0a773b09fd"
        sessionAuthManager.popularAuthToken = "a32b4948-d278-4923-880e-8fb57741c190"
        sessionAuthManager.recentAuthToken = "f5acfd96-384a-4552-a0b5-399675a03826"
        sessionAuthManager.animatorsAuthToken = "5b3e8205-b317-45c4-a5ce-36c9dc57911d"
        sessionAuthManager.blogsAuthToken = "c6bf2153-7a69-4a47-9e3a-6f7500d8f523"
    }

    fun logout() {
        sessionAuthManager.featuredAuthToken = null
        sessionAuthManager.popularAuthToken = null
        sessionAuthManager.recentAuthToken = null
        sessionAuthManager.animatorsAuthToken = null
        sessionAuthManager.blogsAuthToken = null
    }

    fun isUserLoggedIn(): Boolean =
        sessionAuthManager.featuredAuthToken != null &&
                sessionAuthManager.popularAuthToken != null &&
                sessionAuthManager.recentAuthToken != null &&
                sessionAuthManager.blogsAuthToken != null &&
                sessionAuthManager.animatorsAuthToken != null

    fun getFeaturedAnimations() = getAnimationListResponse(
        apiService.getFeaturedAnimations(sessionAuthManager.featuredAuthToken), "featured"
    )

    fun getRecentAnimations() = getAnimationListResponse(
        apiService.getRecentAnimations(sessionAuthManager.recentAuthToken), "recent"
    )

    fun getPopularAnimations() = getAnimationListResponse(
        apiService.getPopularAnimations(sessionAuthManager.popularAuthToken), "popular"
    )

    fun getFeaturedAnimators(): Single<Response<List<Author>>> =
        handleHttpError(apiService.getFeaturedAnimators(sessionAuthManager.animatorsAuthToken)
            .map {
                if (it.data == null) {
                    Response.ErrorResponse(it.code, it.message)
                } else {
                    val itemType = object : TypeToken<List<Author>>() {}.type
                    val authorList: List<Author> = gson.fromJson(
                        it.data.getAsJsonObject("featuredAnimators").getAsJsonArray("results"),
                        itemType
                    )
                    Response.SuccessResponse(authorList)
                }
            }
        )

    fun getBlogs(): Single<Response<List<Blog>>> =
        handleHttpError(apiService.getBlogs(sessionAuthManager.blogsAuthToken)
            .map {
                if (it.data == null) {
                    Response.ErrorResponse(it.code, it.message)
                } else {
                    val itemType = object : TypeToken<List<Blog>>() {}.type
                    val blogsList: List<Blog> = gson.fromJson(
                        it.data.getAsJsonObject("blogs").getAsJsonArray("results"),
                        itemType
                    )
                    Response.SuccessResponse(blogsList)
                }
            }
        )

    /**
     * Maps a NetworkResponse that looks like {"data": { "typeString": { "results": [] } } }
     * And returns either a SuccessResponse<List<AnimationModel>> or an ErrorResponse
     * if the response is anything but 200, it will return the code and the exception message
     * in the ErrorResponse `code` and `message`
     */
    private fun getAnimationListResponse(
        call: Single<NetworkResponse>,
        typeString: String
    ): Single<Response<List<AnimationModel>>> =
        handleHttpError(
            call.map {
                parseAnimationResponse(it, typeString)
            })


    /** if the response is anything but 200, it will return the code and the exception message
     * in the ErrorResponse `code` and `message`
     */
    private fun <T : Any> handleHttpError(single: Single<Response<T>>): Single<Response<T>> {
        return single.onErrorReturn {
            Response.ErrorResponse(
                if (it is HttpException) it.code().toString() else "Unknown", it.message
            )
        }
    }

    /**
     * Parses the NetworkResponse that looks lke {"data": { "typeString": { "results": [] } } }
     * where the content of results can be mapped to List<AnimationModel>
     * Handling HTTP 200 Responses that looks like {"code": "...", "message": "..."}
     * and returns an ErrorResponse with the correct code and message to the caller.
     */
    private fun parseAnimationResponse(networkResponse: NetworkResponse, typeString: String) =
        if (networkResponse.data == null) {
            Response.ErrorResponse(networkResponse.code, networkResponse.message)
        } else {
            val itemType = object : TypeToken<List<AnimationModel>>() {}.type
            val animationList: List<AnimationModel> = gson.fromJson(
                networkResponse.data.getAsJsonObject(typeString)
                    .getAsJsonArray("results"),
                itemType
            )
            Response.SuccessResponse(animationList)
        }
}

