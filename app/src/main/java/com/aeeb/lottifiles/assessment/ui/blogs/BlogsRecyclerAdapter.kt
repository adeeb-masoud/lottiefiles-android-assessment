package com.aeeb.lottifiles.assessment.ui.blogs

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aeeb.lottifiles.assessment.R
import com.aeeb.lottifiles.assessment.network.model.Blog
import com.aeeb.lottifiles.assessment.ui.helper.ImageLoadingHelper

class BlogsRecyclerAdapter : RecyclerView.Adapter<BlogsRecyclerAdapter.ViewHolder>() {

    private val items: MutableList<Blog> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BlogsRecyclerAdapter.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.item_blog, parent, false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BlogsRecyclerAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateItems(items: Collection<Blog>?) {
        if (items.isNullOrEmpty()) return
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageBlog: ImageView = itemView.findViewById(R.id.img_blog)
        private val titleBlog: TextView = itemView.findViewById(R.id.txt_title_blog)
        private val dateBlog: TextView = itemView.findViewById(R.id.txt_date_blog)

        fun bind(blog: Blog) {
            ImageLoadingHelper.loadImage(imageBlog, blog.imageUrl)
            titleBlog.text = blog.title
            dateBlog.text = blog.postedAt?.substring(0, 10)
        }
    }
}
