package com.aeeb.lottifiles.assessment.ui.blogs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aeeb.lottifiles.assessment.R

class BlogsActivity : AppCompatActivity() {
    private val blogsRecyclerAdapter = BlogsRecyclerAdapter()
    private val viewModel: BlogsViewModel by viewModels()
    private lateinit var loadingView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blogs)

        setupRecycler()
        observeViewModel()
        viewModel.initWithContext(applicationContext)
    }

    private fun observeViewModel() {
        viewModel.onError = {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
        viewModel.showLoading.observe(this) {
            loadingView.visibility = if (it) View.VISIBLE else View.GONE
        }
        viewModel.loadedBlogs.observe(this) {
            blogsRecyclerAdapter.updateItems(it)
        }
    }

    override fun onDestroy() {
        viewModel.onError = null
        super.onDestroy()
    }

    private fun setupRecycler() {
        loadingView = findViewById(R.id.blogs_loading_animation)
        val blogsRecycler = findViewById<RecyclerView>(R.id.recycler_blogs)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        blogsRecycler.layoutManager = linearLayoutManager
        blogsRecycler.setHasFixedSize(true)
        blogsRecycler.adapter = blogsRecyclerAdapter
    }
}