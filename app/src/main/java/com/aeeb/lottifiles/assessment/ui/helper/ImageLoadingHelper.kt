package com.aeeb.lottifiles.assessment.ui.helper

import android.animation.Animator
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

object ImageLoadingHelper {

    //Load an image into an image view
    fun loadImage(
        imageView: ImageView,
        url: String?,
        cacheOnDisk: Boolean = true,
        successListener: (() -> Unit)? = null,
        failListener: (() -> Unit)? = null
    ) {
        if (url.isNullOrBlank()) return
        try {
            Uri.parse(url)
        } catch (e: Exception) {
            return
        }
        Glide.with(imageView)
            .load(url)
            .diskCacheStrategy(if (cacheOnDisk) DiskCacheStrategy.ALL else DiskCacheStrategy.NONE)
            .addListener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    failListener?.invoke()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    successListener?.invoke()
                    return false
                }

            })
            .into(imageView)
    }

    //load an animation into an animation view
    fun loadAnimation(
        placeHolderImageView: ImageView,
        lottieAnimationView: LottieAnimationView,
        placeholderImageUrl: String?,
        lottieJsonUrl: String?
    ) {
        loadImage(placeHolderImageView, placeholderImageUrl)

        if (lottieJsonUrl.isNullOrBlank()) return
        try {
            Uri.parse(lottieJsonUrl)
        } catch (e: Exception) {
            return
        }
        lottieAnimationView.setAnimationFromUrl(lottieJsonUrl, lottieJsonUrl)
        lottieAnimationView.addAnimatorListener(object : Animator.AnimatorListener{
            override fun onAnimationStart(p0: Animator?) {
                if (lottieAnimationView.visibility == View.INVISIBLE) {
                    lottieAnimationView.visibility = View.VISIBLE
                    placeHolderImageView.visibility = View.INVISIBLE
                }
            }

            override fun onAnimationEnd(p0: Animator?) {

            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationRepeat(p0: Animator?) {

            }
        })
        lottieAnimationView.playAnimation()
    }
}