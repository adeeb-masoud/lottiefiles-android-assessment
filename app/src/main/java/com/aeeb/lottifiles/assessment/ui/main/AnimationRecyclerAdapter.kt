package com.aeeb.lottifiles.assessment.ui.main

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aeeb.lottifiles.assessment.R
import com.aeeb.lottifiles.assessment.network.model.AnimationModel
import com.aeeb.lottifiles.assessment.ui.helper.ImageLoadingHelper
import com.airbnb.lottie.LottieAnimationView


class AnimationRecyclerAdapter(
    internal val shareTapListener: (AnimationModel) -> Unit
) :
    RecyclerView.Adapter<AnimationRecyclerAdapter.ViewHolder>() {

    private val items: MutableList<AnimationModel> = mutableListOf()

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateItems(collection: Collection<AnimationModel>) {
        items.clear()
        items.addAll(collection)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.item_animation, parent, false
        )
        return ViewHolder(view)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val labelDate = itemView.findViewById<TextView>(R.id.txt_date)
        private val labelAuthor = itemView.findViewById<TextView>(R.id.txt_author)
        private val labelName = itemView.findViewById<TextView>(R.id.txt_name)
        private val imgPreview = itemView.findViewById<ImageView>(R.id.img_preview)
        private val animationView =
            itemView.findViewById<LottieAnimationView>(R.id.lottie_animation_view)
        private val imgAuthor = itemView.findViewById<ImageView>(R.id.img_author)
        private val btnShare = itemView.findViewById<View>(R.id.btn_share)
        init {
            //To fix the illegal state exception
            animationView.imageAssetsFolder = "images/"
        }

        fun bind(animationModel: AnimationModel) {
            labelDate.text = animationModel.createdAt?.substring(0, 10)
            labelAuthor.text = animationModel.author?.name
            labelName.text = animationModel.name
            try {
                animationView.setBackgroundColor(Color.parseColor(animationModel.bgColor))
            } catch (e: IllegalArgumentException) {
                animationView.setBackgroundColor(Color.WHITE)
            }
            ImageLoadingHelper.loadImage(imgAuthor, animationModel.author?.avatarUrl)
            ImageLoadingHelper.loadAnimation(
                imgPreview,
                animationView,
                animationModel.imageUrl,
                animationModel.lottieUrl
            )
            btnShare.setOnClickListener {
                shareTapListener.invoke(animationModel)
            }
        }
    }
}