package com.aeeb.lottifiles.assessment.network.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName


open class NetworkResponse (
    val code: String? = null,
    val message: String? = null,
    val data: JsonObject? = null
)

data class AnimationModel(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("bgColor")
    val bgColor: String = "#FFFFFF",
    @SerializedName("lottieUrl")
    val lottieUrl: String? = null,
    @SerializedName("gifUrl")
    val gifUrl: String? = null,
    @SerializedName("videoUrl")
    val videoUrl: String? = null,
    @SerializedName("imageUrl")
    val imageUrl: String? = null,
    @SerializedName("createdAt")
    val createdAt: String? = "2018-01-01T00:00:00.000z",
    @SerializedName("createdBy")
    val author: Author? = null
)

data class Author(
    @SerializedName("name")
    val name: String = "Unknown",
    @SerializedName("avatarUrl")
    val avatarUrl: String? = null
)

data class Blog(
    @SerializedName("title")
    val title: String = "Unknown",
    @SerializedName("postedAt")
    val postedAt: String? = "2018-01-01T00:00:00.000z",
    @SerializedName("imageUrl")
    val imageUrl: String? = null
)