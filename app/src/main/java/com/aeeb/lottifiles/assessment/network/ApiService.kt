package com.aeeb.lottifiles.assessment.network

import com.aeeb.lottifiles.assessment.network.model.NetworkResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("featuredAnimations.json?alt=media")
    fun getFeaturedAnimations(@Query("token") token: String? = null): Single<NetworkResponse>

    @GET("popularAnimations.json?alt=media")
    fun getPopularAnimations(@Query("token") token: String? = null): Single<NetworkResponse>

    @GET("recentAnimations.json?alt=media")
    fun getRecentAnimations(@Query("token") token: String? = null): Single<NetworkResponse>

    @GET("featuredAnimators.json?alt=media")
    fun getFeaturedAnimators(@Query("token") token: String? = null): Single<NetworkResponse>

    @GET("blogs.json?alt=media")
    fun getBlogs(@Query("token") token: String? = null): Single<NetworkResponse>
}