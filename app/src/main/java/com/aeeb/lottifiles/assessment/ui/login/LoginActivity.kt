package com.aeeb.lottifiles.assessment.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.aeeb.lottifiles.assessment.R
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatButton
import com.aeeb.lottifiles.assessment.ui.main.MainActivity


class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModels()

    private lateinit var loginButton: View
    private lateinit var loadingView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initViews()

        loginViewModel.initViewModel(applicationContext)
        loginViewModel.isLoggingIn.observe(this) { loggingIn ->
            if (loggingIn) {
                loginButton.visibility = View.GONE
                loadingView.visibility = View.VISIBLE
            }
        }
        loginViewModel.isLoggedIn.observe(this) { loggedIn ->
            if (loggedIn) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
        loginViewModel.hasThrownAnError.observe(this) {
            if (it)
                Toast.makeText(this, "An unknown error has occurred", Toast.LENGTH_LONG).show()
        }

    }

    private fun initViews() {
        loginButton = findViewById(R.id.btn_login)
        loadingView = findViewById(R.id.animation_view_loading)
        loginButton.setOnClickListener { loginViewModel.login() }
    }
}