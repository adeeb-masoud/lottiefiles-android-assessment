package com.aeeb.lottifiles.assessment.network

import android.content.Context
import android.content.SharedPreferences
import com.aeeb.lottifiles.assessment.R

class SessionAuthManager(context: Context) {
    companion object {
        private const val FEATURED_TOKEN = "FEATURED_TOKEN"
        private const val RECENT_TOKEN = "RECENT_TOKEN"
        private const val POPULAR_TOKEN = "POPULAR_TOKEN"
        private const val ANIMATORS_TOKEN = "ANIMATORS_TOKEN"
        private const val BLOGS_TOKEN = "BLOGS_TOKEN"
    }

    private var prefs: SharedPreferences =
        context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)

    var featuredAuthToken: String?
        get() {
            return getToken(FEATURED_TOKEN)
        }
        set(value) {
            setToken(FEATURED_TOKEN, value)
        }

    var recentAuthToken: String?
        get() {
            return getToken(RECENT_TOKEN)
        }
        set(value) {
            setToken(RECENT_TOKEN, value)
        }

    var popularAuthToken: String?
        get() {
            return getToken(POPULAR_TOKEN)
        }
        set(value) {
            setToken(POPULAR_TOKEN, value)
        }

    var animatorsAuthToken: String?
        get() {
            return getToken(ANIMATORS_TOKEN)
        }
        set(value) {
            setToken(ANIMATORS_TOKEN, value)
        }

    var blogsAuthToken: String?
        get() {
            return getToken(BLOGS_TOKEN)
        }
        set(value) {
            setToken(BLOGS_TOKEN, value)
        }

    private fun setToken(keyName: String, keyValue: String?) {
        prefs.edit()
            .putString(keyName, keyValue)
            .apply()
    }

    private fun getToken(keyName: String) = prefs.getString(keyName, null)
}