package com.aeeb.lottifiles.assessment.ui.login

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aeeb.lottifiles.assessment.network.NetworkClient
import com.aeeb.lottifiles.assessment.network.SessionAuthManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class LoginViewModel : ViewModel() {
    val isLoggingIn = MutableLiveData(false)
    val isLoggedIn = MutableLiveData(false)
    val hasThrownAnError = MutableLiveData(false)

    private lateinit var loginDisposable: Disposable
    private lateinit var networkClient: NetworkClient

    fun initViewModel(applicationContext: Context) {
        this.networkClient =
            NetworkClient(applicationContext, SessionAuthManager(applicationContext))
        if (networkClient.isUserLoggedIn())
            login()

    }

    fun login() {
        isLoggingIn.value = true
        networkClient.login()
        loginDisposable = Single.timer(4, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { isLoggingIn.value = false }
            .subscribe ({
                isLoggedIn.value = true
            }, {
                hasThrownAnError.value = true
            })
    }

}
