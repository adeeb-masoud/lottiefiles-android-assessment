package com.aeeb.lottifiles.assessment.ui.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aeeb.lottifiles.assessment.R
import com.aeeb.lottifiles.assessment.ui.blogs.BlogsActivity
import com.aeeb.lottifiles.assessment.ui.helper.ImageLoadingHelper
import com.aeeb.lottifiles.assessment.ui.login.LoginActivity
import com.airbnb.lottie.LottieAnimationView
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var animationRecyclerAdapter: AnimationRecyclerAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager

    private lateinit var animationsRecycler: RecyclerView
    private lateinit var animationsRecyclerLoading: LottieAnimationView

    private lateinit var featuredButton: AppCompatButton
    private lateinit var recentButton: AppCompatButton
    private lateinit var popularButton: AppCompatButton

    private lateinit var animatorGroup: View
    private lateinit var animatorLoadingView: View
    private lateinit var animatorImage: ImageView
    private lateinit var animatorName: TextView

    private lateinit var blogsLoading: View
    private lateinit var blog1Image: ImageView
    private lateinit var blog1Title: TextView
    private lateinit var blog1Date: TextView
    private lateinit var blog1Group: View
    private lateinit var blog2Image: ImageView
    private lateinit var blog2Title: TextView
    private lateinit var blog2Date: TextView
    private lateinit var blog2Group: View
    private lateinit var blog3Image: ImageView
    private lateinit var blog3Title: TextView
    private lateinit var blog3Date: TextView
    private lateinit var blog3Group: View
    private lateinit var blog4Image: ImageView
    private lateinit var blog4Title: TextView
    private lateinit var blog4Date: TextView
    private lateinit var blog4Group: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        initViews()
        setupRecyclerView()
        setupToolbar()

        viewModel.initWithAppContext(applicationContext)

        observeCatastrophicFailure()
        observeLogout()
        observeAnimations()
        observeFeaturedAnimators()
        observeBlogs()
    }

    private fun observeCatastrophicFailure() {
        viewModel.hasThrownCatastrophicFailure.observe(this) {
            if (it)
                Toast.makeText(
                    this,
                    "Catastrophic Error. This should have not happened!",
                    Toast.LENGTH_SHORT
                ).show()
        }
    }

    private fun observeLogout() {
        viewModel.isUserLoggedOut.observe(this) {
            if (it) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
    }

    private fun observeAnimations() {
        //loading state
        viewModel.areAnimationsLoading.observe(this) { loading ->
            if (loading)
                animationsRecyclerLoading.playAnimation()
            animationsRecycler.visibility = if (loading) View.INVISIBLE else View.VISIBLE
            animationsRecyclerLoading.visibility = if (loading) View.VISIBLE else View.GONE
        }
        //selected animation type
        viewModel.selectedAnimationType.observe(this) {

            featuredButton.setOnClickListener { viewModel.onFeaturedBtnClicked() }
            recentButton.setOnClickListener { viewModel.onRecentBtnClicked() }
            popularButton.setOnClickListener { viewModel.onPopularBtnClicked() }

            //set all buttons to not selected
            featuredButton.setTextColor(ContextCompat.getColor(this, R.color.primary_dark))
            recentButton.setTextColor(ContextCompat.getColor(this, R.color.primary_dark))
            popularButton.setTextColor(ContextCompat.getColor(this, R.color.primary_dark))
            featuredButton.backgroundTintList = ContextCompat.getColorStateList(this, R.color.white)
            recentButton.backgroundTintList = ContextCompat.getColorStateList(this, R.color.white)
            popularButton.backgroundTintList = ContextCompat.getColorStateList(this, R.color.white)

            //get the selected button
            val selectedButton = when (it) {
                AnimationsType.FEATURED -> featuredButton
                AnimationsType.RECENT -> recentButton
                AnimationsType.POPULAR -> popularButton
                null -> featuredButton
            }
            //invert the selected button colors
            selectedButton.setTextColor(ContextCompat.getColor(this, R.color.white))
            selectedButton.backgroundTintList =
                ContextCompat.getColorStateList(this, R.color.primary_dark)
        }

        //observe the loaded animations changes
        viewModel.currentAnimations.observe(this) {
            animationRecyclerAdapter.updateItems(it)
            linearLayoutManager.scrollToPositionWithOffset(0, 0)
        }
    }

    private fun observeFeaturedAnimators() {

        //observe loading state
        viewModel.areAnimatorsLoading.observe(this) { loading ->
            animatorGroup.visibility = if (loading) View.INVISIBLE else View.VISIBLE
            animatorLoadingView.visibility = if (loading) View.VISIBLE else View.GONE
        }
        //observe loaded animators
        viewModel.currentAnimator.observe(this) {
            animatorName.text = it.name
            ImageLoadingHelper.loadImage(animatorImage, it.avatarUrl)
        }

    }

    private fun observeBlogs() {
        //observe blogs loading
        viewModel.areBlogsLoading.observe(this) { loading ->
            blogsLoading.visibility = if (loading) View.VISIBLE else View.GONE
        }
        //observe loaded blogs
        viewModel.loadedBlogs.observe(this) {
            //fill the first blog
            if (it.isNotEmpty()) {
                blog1Group.visibility = View.VISIBLE
                blog1Title.text = it[0].title
                blog1Date.text = it[0].postedAt?.substring(0, 10)
                ImageLoadingHelper.loadImage(blog1Image, it[0].imageUrl)
            }
            //fill the second blog
            if (it.size > 1) {
                blog2Group.visibility = View.VISIBLE
                blog2Title.text = it[1].title
                blog2Date.text = it[1].postedAt?.substring(0, 10)
                ImageLoadingHelper.loadImage(blog2Image, it[1].imageUrl)
            }
            //fill the third blog
            if (it.size > 2) {
                blog3Group.visibility = View.VISIBLE
                blog3Title.text = it[2].title
                blog3Date.text = it[2].postedAt?.substring(0, 10)
                ImageLoadingHelper.loadImage(blog3Image, it[2].imageUrl)
            }
            //fill the fourth blog
            if (it.size > 3) {
                blog4Group.visibility = View.VISIBLE
                blog4Title.text = it[3].title
                blog4Date.text = it[3].postedAt?.substring(0, 10)
                ImageLoadingHelper.loadImage(blog4Image, it[3].imageUrl)
            }

        }
    }

    private fun initViews() {

        findViewById<View>(R.id.btn_logout).setOnClickListener { viewModel.onLogoutButtonClicked() }
        findViewById<View>(R.id.txt_show_all_blogs).setOnClickListener {
            startActivity(Intent(this, BlogsActivity::class.java))
        }

        animationsRecycler = findViewById(R.id.recycler_animation)
        animationsRecyclerLoading = findViewById(R.id.recycler_animation_loading)

        featuredButton = findViewById(R.id.btn_featured)
        recentButton = findViewById(R.id.btn_recent)
        popularButton = findViewById(R.id.btn_popular)

        animatorLoadingView = findViewById(R.id.featured_animator_loading)
        animatorGroup = findViewById(R.id.group_featured_animators)
        animatorImage = findViewById(R.id.img_featured_animator)
        animatorName = findViewById(R.id.txt_featured_animator)

        blogsLoading = findViewById(R.id.blogs_loading)

        blog1Image = findViewById(R.id.img_blog_1)
        blog1Title = findViewById(R.id.txt_title_blog_1)
        blog1Date = findViewById(R.id.txt_date_blog_1)
        blog1Group = findViewById(R.id.group_blog_1)

        blog2Image = findViewById(R.id.img_blog_2)
        blog2Title = findViewById(R.id.txt_title_blog_2)
        blog2Date = findViewById(R.id.txt_date_blog_2)
        blog2Group = findViewById(R.id.group_blog_2)

        blog3Image = findViewById(R.id.img_blog_3)
        blog3Title = findViewById(R.id.txt_title_blog_3)
        blog3Date = findViewById(R.id.txt_date_blog_3)
        blog3Group = findViewById(R.id.group_blog_3)

        blog4Image = findViewById(R.id.img_blog_4)
        blog4Title = findViewById(R.id.txt_title_blog_4)
        blog4Date = findViewById(R.id.txt_date_blog_4)
        blog4Group = findViewById(R.id.group_blog_4)
    }

    private fun setupRecyclerView() {
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        animationsRecycler.layoutManager =
            linearLayoutManager
        animationsRecycler.setHasFixedSize(true)
        animationRecyclerAdapter = AnimationRecyclerAdapter { animation ->
            //TODO: download video and share the downloaded file instead
            val uri: Uri
            try {
                uri = Uri.parse(animation.videoUrl)
            } catch (e: Exception) {
                Toast.makeText(this, "Unable to share animation", Toast.LENGTH_SHORT).show()
                return@AnimationRecyclerAdapter
            }
            val sendIntent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, animation.name)
                putExtra(Intent.EXTRA_STREAM, uri)
                type = "video/mp4"
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            startActivity(Intent.createChooser(sendIntent, "Share Animation"))
        }
        animationsRecycler.adapter = animationRecyclerAdapter
    }

    private fun setupToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)
    }
}