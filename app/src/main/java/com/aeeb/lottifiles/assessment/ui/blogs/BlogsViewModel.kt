package com.aeeb.lottifiles.assessment.ui.blogs

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aeeb.lottifiles.assessment.network.NetworkClient
import com.aeeb.lottifiles.assessment.network.SessionAuthManager
import com.aeeb.lottifiles.assessment.network.model.Blog
import com.aeeb.lottifiles.assessment.network.model.Response
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class BlogsViewModel : ViewModel() {

    val showLoading = MutableLiveData(false)
    val loadedBlogs = MutableLiveData<List<Blog>>()
    var onError: ((String) -> Unit)? = null

    private val disposable = CompositeDisposable()

    fun initWithContext(applicationContext: Context) {
        //blogs already loaded
        if (loadedBlogs.value != null) return
        showLoading.value = true
        disposable.add(NetworkClient(applicationContext, SessionAuthManager(applicationContext))
            .getBlogs()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { showLoading.value = false }
            .subscribe({
                when (it) {
                    is Response.ErrorResponse -> {
                        onError?.invoke("Error loading blogs: ${it.message ?: ""}")
                    }
                    is Response.SuccessResponse -> loadedBlogs.value = it.value
                }
            },
                {
                    onError?.invoke("Catastrophic failure!")
                })
        )
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }
}
