package com.aeeb.lottifiles.assessment

import android.app.Application
import com.airbnb.lottie.Lottie
import com.airbnb.lottie.LottieConfig

class LottieFilesAssessmentApp: Application() {
    override fun onCreate() {
        super.onCreate()
        Lottie.initialize(
        LottieConfig.Builder()
            .setNetworkCacheDir(cacheDir)
            .build()
        )
    }
}