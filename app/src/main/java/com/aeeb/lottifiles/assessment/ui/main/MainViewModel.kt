package com.aeeb.lottifiles.assessment.ui.main

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aeeb.lottifiles.assessment.network.NetworkClient
import com.aeeb.lottifiles.assessment.network.SessionAuthManager
import com.aeeb.lottifiles.assessment.network.model.AnimationModel
import com.aeeb.lottifiles.assessment.network.model.Author
import com.aeeb.lottifiles.assessment.network.model.Blog
import com.aeeb.lottifiles.assessment.network.model.Response
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainViewModel : ViewModel() {

    private lateinit var networkClient: NetworkClient
    private val disposable = CompositeDisposable()
    private var animationDisposable: Disposable? = null

    private val loadedAnimators = MutableLiveData<List<Author>>()
    private var currentAnimatorOnDisplay = 0

    val selectedAnimationType = MutableLiveData(AnimationsType.FEATURED)
    val currentAnimations = MutableLiveData<List<AnimationModel>>()
    val currentAnimator = MutableLiveData<Author>()
    val loadedBlogs = MutableLiveData<List<Blog>>()
    val areAnimatorsLoading = MutableLiveData(false)
    val areAnimationsLoading = MutableLiveData(false)
    val areBlogsLoading = MutableLiveData(false)
    val isUserLoggedOut = MutableLiveData(false)
    val hasThrownCatastrophicFailure = MutableLiveData(false)

    fun initWithAppContext(applicationContext: Context) {
        //Dependencies can be injected here instead. But for simplicity, they were constructed here
        this.networkClient =
            NetworkClient(applicationContext, SessionAuthManager(applicationContext))
        loadData()
    }

    private fun loadData() {
        if (currentAnimations.value.isNullOrEmpty())
            loadAnimations()
        if (loadedAnimators.value.isNullOrEmpty())
            loadAnimators()
        if (loadedBlogs.value.isNullOrEmpty())
            loadBlogs()
    }

    private fun loadBlogs() {
        areBlogsLoading.value = true
        disposable.add(networkClient.getBlogs()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { areBlogsLoading.value = false }
            .subscribe({
                when (it) {
                    is Response.ErrorResponse -> handleError(it)
                    is Response.SuccessResponse -> loadedBlogs.value = it.value
                }
            },
                {
                    hasThrownCatastrophicFailure.value = true
                })
        )

    }

    private fun handleError(response: Response.ErrorResponse) {
        //user is not authorised
        if (response.code == "403") {
            isUserLoggedOut.value = true
        }
    }

    private fun loadAnimators() {
        areAnimatorsLoading.value = true
        disposable.add(networkClient.getFeaturedAnimators()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { areAnimatorsLoading.value = false }
            .subscribe({
                when (it) {
                    is Response.ErrorResponse -> handleError(it)
                    is Response.SuccessResponse -> {
                        loadedAnimators.value = it.value
                        startLoopingAnimators()
                    }
                }
            },
                {
                    hasThrownCatastrophicFailure.value = true
                })
        )
    }

    private fun startLoopingAnimators() {
        val currentAnimators = loadedAnimators.value
        if (currentAnimators.isNullOrEmpty()) return
        currentAnimator.value = currentAnimators[0]
        disposable.add(Observable.interval(4, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                currentAnimator.value =
                    currentAnimators[++currentAnimatorOnDisplay % currentAnimators.size]
            }, {
                hasThrownCatastrophicFailure.value = true
            })
        )
    }

    private fun loadAnimations() {
        if (animationDisposable != null && !animationDisposable!!.isDisposed) {
            animationDisposable?.dispose()
            animationDisposable = null
        }
        areAnimationsLoading.value = true
        val api = when (selectedAnimationType.value) {
            AnimationsType.FEATURED -> networkClient.getFeaturedAnimations()
            AnimationsType.RECENT -> networkClient.getRecentAnimations()
            AnimationsType.POPULAR -> networkClient.getPopularAnimations()
            null -> networkClient.getFeaturedAnimations()
        }
        animationDisposable = api.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { areAnimationsLoading.value = false }
            .subscribe({
                when (it) {
                    is Response.ErrorResponse -> handleError(it)
                    is Response.SuccessResponse -> currentAnimations.value = it.value
                }
            }, {
                hasThrownCatastrophicFailure.value = true
            })

        disposable.add(animationDisposable!!)
    }

    fun onFeaturedBtnClicked() {
        if (selectedAnimationType.value == AnimationsType.FEATURED) return
        selectedAnimationType.value = AnimationsType.FEATURED
        loadAnimations()
    }

    fun onRecentBtnClicked() {
        if (selectedAnimationType.value == AnimationsType.RECENT) return
        selectedAnimationType.value = AnimationsType.RECENT
        loadAnimations()
    }

    fun onPopularBtnClicked() {
        if (selectedAnimationType.value == AnimationsType.POPULAR) return
        selectedAnimationType.value = AnimationsType.POPULAR
        loadAnimations()
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }

    fun onLogoutButtonClicked() {
        networkClient.logout()
        isUserLoggedOut.value= true
    }
}

enum class AnimationsType {
    FEATURED, RECENT, POPULAR
}
