package com.aeeb.lottifiles.assessment.network.model

sealed class Response<out T: Any> {
    data class SuccessResponse<out T: Any>(val value: T): Response<T>()
    data class ErrorResponse(val code: String?, val message: String?): Response<Nothing>()
}