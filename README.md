## Lottiefiles Android assessment

This repository implements lottiefiles assessment using MVVM and clean architecture.and

The project consists of 3 screens:

1. **Login Page:** A simple fake login page that fills SharedPreferences with the auth tokens. It also waits 4 seconds before opening the main page.
2. **Main Page:** This is the main page of the app. It loads all payloads and displays them in sections.
3. **Blogs Page:** This page is displayed when the user taps "see all" on the main page.

### Application decisions:

The app is designed and implemented based on the idea that the content is fixed. And loading it one time is enough.
Glide is used for loading and caching images. Okhttp and Retrofit are used for networking, Okhttp cache is used for caching requests. And lottie SDK is used for caching animations.

In a production app, I would have used Hilt or Dagger for dependency injection. And also used factories for ViewModels. There would have been unit tests for the code as well. And I would have downloaded the video before sharing it to a stream.
For saving time and for simplicity, I skipped on the above.

However, the app works well in both orientations. And it does what it supposed to do (Except sharing that doesn't work very well). And it is well structured and testable.

I hope you enjoy reading the code as much as I enjoyed writing it. I used some old-school techniques when writing it.